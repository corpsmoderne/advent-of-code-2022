mod misc;

use misc::{get_data, build_map, build_graph, find_path, print_path};

fn main() {
    let lines = get_data("./day12/input");
    let Some((map, start, end)) = build_map(&lines) else {
        panic!("bad map file");
    };

    let graph = build_graph(&map); 
    println!("start: {:?} , end: {:?}", start, end);
    
    let path = find_path(vec![start], end, &graph).unwrap();

    print_path(&lines, start, end, &path);
    
    println!("Result: {}", path.len());
    println!();
}
