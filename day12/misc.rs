use std::fs::File;
use std::io::{self,BufRead};
use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;

pub type Pos = (i16, i16);
type Map = HashMap<Pos, u8>;
type Graph = HashMap<Pos, Vec<Pos>>;

type Path = Vec<Pos>;

pub fn build_map(lines: &[String]) -> Option<(Map, Pos, Pos)> {
    let mut map : Map = HashMap::new();
    let mut start : Option<Pos> = None;
    let mut end : Option<Pos> = None;
    
    for (y, line) in lines.iter().enumerate() {
        for (x, c) in line.bytes().enumerate() {
            let c = match c {
                b'S' => { start = Some((x as i16, y as i16)); b'a' },
                b'E' => { end = Some((x as i16, y as i16)); b'z' },
                _ => c
            };
            map.insert((x as i16, y as i16) , c);
        }
    }
    match (start, end) {
        (Some(start), Some(end)) => Some((map, start, end)),
        _ => None
    }
}

pub fn build_graph(map: &Map) -> Graph {
    let mut map2 = HashMap::new();
    
    for (k, v) in map {
        let to_go : Path =
            ([ (k.0, k.1+1), (k.0, k.1-1), (k.0-1, k.1), (k.0+1, k.1) ])
            .iter()
            .filter(| (x, y) | {
                let Some(v1) = map.get(&(*x, *y)) else {
                    return false;
                };
                (*v1 as i16 - *v as i16) <= 1
            })
            .copied()
            .collect();
        map2.insert(*k, to_go);
    }
    map2
}

pub fn find_path(starts: Path, end: Pos, map: &Graph) -> Option<Path> {
    let mut visited : HashSet<Pos> = HashSet::new();
    let mut paths : HashMap<Pos, Pos> = HashMap::new();
    for start in &starts {
        visited.insert(*start);
    }
    let mut q : VecDeque<Pos> = VecDeque::from(starts);
    
    while let Some(node) = q.pop_back() { 
        if node == end {
            break;
        }
        for to in &map[&node] {
            if visited.contains(to) {
                continue;
            }
            visited.insert(*to);
            paths.insert(*to, node);
            q.push_front(*to);
        }
    }
    build_path(paths, end)
}

fn build_path(paths: HashMap<Pos, Pos>, end: Pos) -> Option<Path> {
    let mut u = paths.get(&end);
    u?;
    let mut path = vec![];
    while let Some(node) = u {
	path.push(*node);
	u = paths.get(node);
    }
    path.reverse();
    Some(path)
}

pub fn print_path(
    lines: &Vec<String>,
    start: Pos,
    end: Pos,
    path: &Path
) {
    let mut map3 : Vec<Vec<char>> = vec![];
    for line in lines {
	map3.push(vec!['.' ; line.len()]);
    }
    map3[start.1 as usize][start.0 as usize] = 'S';
    map3[end.1 as usize][end.0 as usize] = 'E';
    for i in 1..path.len()-1 {
	let node = path[i];
	let node1 = path[i+1];
	
	if node.0 < node1.0 {
	    map3[node.1 as usize][node.0 as usize] = '>'
	} else if node.0 > node1.0 {
	    map3[node.1 as usize][node.0 as usize] = '<'
	} else if node.1 < node1.1 {
	    map3[node.1 as usize][node.0 as usize] = 'v'
	} else {
	    map3[node.1 as usize][node.0 as usize] = '^'
	}
        
    }
    for line in map3 {
	for c in line {
	    print!("{}", c);
	}
	println!()
    }
}

pub fn get_data(filename: &str) -> Vec<String> {
    let file = File::open(filename).unwrap();
    io::BufReader::new(file).lines()
	.map(|l| l.unwrap())
	.collect()
}

