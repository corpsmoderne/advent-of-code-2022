mod misc;

use misc::{get_data, build_map, build_graph, find_path, print_path, Pos};

fn main() {
    let lines = get_data("./day12/input");
    let Some((map, start, end)) = build_map(&lines) else {
        panic!("bad map file");
    };
    let graph = build_graph(&map);

    let candidates : Vec<Pos> = map.iter()
	.filter(| (_k, v) | **v == b'a')
	.map(| (k, _v) | *k)
	.collect();

    let path = find_path(candidates, end, &graph).unwrap();

    /*
    let mut results = vec![];
    for k in &candidates {
	let res = find_path(vec![*k], end, &graph);
	let Some(path) = res else {
	    continue
	};
	results.push(path)
    }
    results.sort_by(| p1, p2 | p1.len().partial_cmp(&p2.len()).unwrap());
    let path = &mut results[0];
    */

    print_path(&lines, start, end, &path);
    println!("Result: {}", path.len());
}
    
