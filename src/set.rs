use std::ops::{BitAnd,BitOr};
use std::convert::From;
use std::fmt::Display;

///
/// Kind of a Set (doesn't preserve ordering)
/// Used on day03 alternative versions (main5.rs and main6.rs)
/// and day04
///

#[derive(Debug,Clone,Copy,PartialEq,Eq)]
pub struct Set {
    tbl: u128
}

impl Set {

    pub fn new() -> Self {
	Self { tbl: 0 }
    }

    pub fn iter(&self) -> impl Iterator<Item = u8> + '_ {
	SetIter { set: self, idx: 0 }
    }

    pub fn set(&mut self, idx: u8) {
	self.tbl |= 1 << idx;
    }

    pub fn clear(&mut self, idx: u8) {
	self.tbl &= !( 1 << idx);
    }

    pub fn is_empty(&self) -> bool {
	self.tbl == 0
    }    
}

impl Default for Set {
    fn default() -> Self {
        Self::new()
    }
}

impl Display for Set {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
	for i in 0..8 {
	    let tmp = (( self.tbl << (i * 16)) & 0xff) as u16;
	    writeln!(f, "{:032b}", tmp)?
	}
	Ok(())
    }
}

impl From<&[u8]> for Set {
    fn from(v: &[u8]) -> Self {
	let mut tbl = 0;
	
	for b in v {
	    tbl |= 1 << *b;
	}
	Self { tbl }
    }
}

impl From<&Vec<u8>> for Set {
    fn from(v: &Vec<u8>) -> Self {
	Self::from(v.as_slice())
    }
}

impl BitAnd for Set {
    type Output = Self;
    
    fn bitand(self, rhs: Self) -> Self {
	Self { tbl: self.tbl & rhs.tbl }
    }
}

impl BitOr for Set {
    type Output = Self;
    
    fn bitor(self, rhs: Self) -> Self {
	Self { tbl: self.tbl | rhs.tbl }
    }
}


pub struct SetIter<'a> {
    set: &'a Set,
    idx: usize
}

impl<'a> Iterator for SetIter<'a> {
    type Item = u8;
    
    fn next(&mut self) -> Option<Self::Item> {
	while self.idx < std::mem::size_of::<u128>()*8 {
	    if self.set.tbl & (1 << self.idx) != 0 {
		let tmp = self.idx as u8;
		self.idx += 1;
		return Some(tmp as u8);
	    }
	    self.idx += 1;
	}
	None
    }
}
