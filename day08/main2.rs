mod misc;

use misc::{get_data,get_tree_map};

fn main() {
    let lines = get_data("./day08/input");

    let mut map = get_tree_map(&lines);

    // calc left
    for y in 0..lines.len() {
	for x in 0..lines[y].len() {
	    let mut count = 0;
	    let val = map[&(x,y)].val;
	    for j in (x+1)..lines[y].len() {
		count += 1;
		let cell = map[&(j,y)];
		if cell.val >= val {
		    break;
		}
	    }
	    let cell = map.get_mut(&(x, y)).unwrap();
	    cell.left = count;
	}
    }
    
    // calc right
    for y in 0..lines.len() {
	for x in 1..lines[y].len() {
	    let mut count = 0;
	    let val = map[&(x,y)].val;
	    for j in (0..=(x-1)).rev() {
		let cell = map[&(j,y)];
		count += 1;
		if cell.val >= val {
		    break;
		}
	    }
	    let cell = map.get_mut(&(x, y)).unwrap();
	    cell.right = count;
	}
    }

    // calc top
    for y in 0..lines.len() {
	for x in 0..lines[y].len() {
	    let mut count = 0;
	    let val = map[&(x,y)].val;
	    for j in (y+1)..lines.len() {
		let cell = map[&(x,j)];
		count += 1;
		if cell.val >= val {
		    break;
		}
	    }
	    let cell = map.get_mut(&(x, y)).unwrap();
	    cell.top = count;
	}
    }

    // calc bottom
    for y in 1..lines.len() {
	for x in 0..lines[y].len() {
	    let mut count = 0;
	    let val = map[&(x,y)].val;
	    for j in (0..=(y-1)).rev() {
		let cell = map[&(x,j)];
		count += 1;
		if cell.val >= val {
		    break;
		}
	    }
	    let cell = map.get_mut(&(x, y)).unwrap();
	    cell.bottom = count;
	}
    }

    let best_tree : u32 = map.iter()
	.map(| (_, cell) | cell.top as u32 *
	     cell.bottom as u32 *
	     cell.left as u32 *
	     cell.right as u32 )
	.max().unwrap();
    println!("{:?}", best_tree);
}
