use std::fs::File;
use std::io::{self,BufRead};
use std::collections::HashMap;

#[derive(Debug,Clone,Copy)]
pub struct Cell {
    pub val: u8,
    pub top: u8,
    pub bottom: u8,
    pub left: u8,
    pub right: u8,
    pub visible: bool
	
}

type Map = HashMap<(usize, usize), Cell>;

pub fn get_tree_map(lines: &[String]) -> Map {
    let mut map = Map::new();
    
    for (y, line) in lines.iter().enumerate() {
	for (x, val) in line.bytes().enumerate() {
	    map.insert((x, y),
		       Cell { val: val - b'0',
			      top: 0, bottom: 0,
			      left: 0, right: 0,
			      visible: true });
	}
    }

    map
}

pub fn get_data(filename: &str) -> Vec<String> {
    let file = File::open(filename).unwrap();
    io::BufReader::new(file).lines()
	.map(|l| l.unwrap())
	.collect()
}

