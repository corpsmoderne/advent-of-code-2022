mod misc;

use misc::{get_data,get_tree_map};

fn main() {
    let lines = get_data("./day08/input");

    let mut map = get_tree_map(&lines);

    // calc left
    for (y, line) in lines.iter().enumerate() {
	let mut max = 0;
	for x in 0..line.len() {
	    let mut cell = map.get_mut(&(x,y)).unwrap(); 
	    max = max.max(cell.val);
	    cell.left = max;
	}
    }

    // calc right
    for (y, line) in lines.iter().enumerate() { //0..lines.len() {
	let mut max = 0;
	for x in (0..line.len()).rev() {
	    let mut cell = map.get_mut(&(x,y)).unwrap(); 
	    max = max.max(cell.val);
	    cell.right = max;
	}
    }
    
    // calc top
    for x in 0..lines[0].len() {
	let mut max = 0;
	for y in 0..lines.len() {
	    let mut cell = map.get_mut(&(x,y)).unwrap(); 
	    max = max.max(cell.val);
	    cell.top = max;
	}
    }

    // calc bottom
    for x in 0..lines[0].len() {
	let mut max = 0;
	for y in (0..lines.len()).rev() {
	    let mut cell = map.get_mut(&(x,y)).unwrap(); 
	    max = max.max(cell.val);
	    cell.bottom = max;
	}
    }

    for y in 1..lines.len()-1 {
	for x in 1..lines[y].len()-1 {
	    let val = {
		let mut cell = map.get_mut(&(x,y)).unwrap();
		cell.visible = false;
		cell.val
	    };

	    if map[&(x-1,y)].left < val || map[&(x+1,y)].right < val ||
		map[&(x,y-1)].top < val || map[&(x,y+1)].bottom < val {
		    let mut cell = map.get_mut(&(x,y)).unwrap();
		    cell.visible = true;
		}
	}
    }
    
    for y in 0..lines.len() {
	for x in 0..lines[y].len() {
	    print!("{} ", map[&(x,y)].val);
	}
	println!();
    }
    println!();    

    for y in 0..lines.len() {
	for x in 0..lines[y].len() {
	    print!("{} ", if map[&(x,y)].visible { 'v' } else { '.' });
	}
	println!();
    }
    println!();    

    let count = map.iter()
	.filter(| (_, cell) | cell.visible)
	.count();
    println!("{count}");
}
