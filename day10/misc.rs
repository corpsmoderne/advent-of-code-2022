use std::fs::File;
use std::io::{self,BufRead};
use std::collections::VecDeque;

#[derive(Debug,Clone,Copy)]
pub enum OpCode {
    Noop,
    Addx(i32)
}

#[derive(Debug,Clone)]
pub struct Cpu {
    pub reg: i32,
    pub cycle: usize,
    current_op: Option<OpCode>,
    eoi: usize, // end of instruction
    prog: VecDeque<OpCode>
}

impl Cpu {
    pub fn new(prog: Vec<OpCode>) -> Cpu {
	Cpu { reg: 1, cycle: 0, current_op: None,
	      eoi: 0, prog: VecDeque::from(prog) }
    }
    
    fn load_next_opcode(&mut self) -> bool {
	self.current_op = self.prog.pop_front();
	match self.current_op {
	    Some(OpCode::Noop) => { self.eoi = 0; true },
	    Some(OpCode::Addx(_)) => { self.eoi = 1; true },
	    None => false
	}
    }
    
    pub fn step(&mut self) -> bool {
	self.cycle += 1;
	
	if self.eoi > 0 {
	    self.eoi -= 1;
	    return true;
	}
	
	let Some(op) = self.current_op else {
	    return self.load_next_opcode()
	};
	
	match op {
	    OpCode::Noop => {},
	    OpCode::Addx(n) => { self.reg += n }
	}
	self.load_next_opcode();
	true
    }
}

impl std::fmt::Display for Cpu {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f,"cpu: #{} / reg:{} / {:?}",
	       self.cycle, self.reg, self.current_op)
    }
}

pub fn load_prog(lines: &Vec<String>) -> Vec<OpCode> {
    let mut prog: Vec<OpCode> = vec![];
    
    for line in lines {
	let op = match &line.split(' ').collect::<Vec<&str>>()[..] {
	    ["noop"] => OpCode::Noop,
	    ["addx", n] => OpCode::Addx(n.parse().unwrap()),
	    _ => panic!("bad opcode")
	};
	prog.push(op);
    }
    prog
}
		 
pub fn get_data(filename: &str) -> Vec<String> {
    let file = File::open(filename).unwrap();
    io::BufReader::new(file).lines()
	.map(|l| l.unwrap())
	.collect()
}

