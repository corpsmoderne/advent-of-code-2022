mod misc;

use misc::{get_data, load_prog, Cpu};

fn main() {
    let lines = get_data("./day10/input0");

    let mut cpu = Cpu::new(load_prog(&lines));
    let mut sum = 0;
    while cpu.step() {
	println!("{} -- {}", cpu, sum);
	if [20,60,100,140,180,220].contains(&cpu.cycle) {
	    let val = cpu.cycle as i32 * cpu.reg;
	    sum += val;
	    println!("*** {} * {} = {} : {}", cpu.cycle, cpu.reg, val, sum);
	}
    }
    println!("{}", sum);
}
