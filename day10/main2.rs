mod misc;

use misc::{get_data, load_prog, Cpu};

fn main() {
    let lines = get_data("./day10/input0");

    let mut cpu = Cpu::new(load_prog(&lines));
    let mut x = 0;
    while cpu.step() {
	if (cpu.reg - x as i32).abs() <= 1 {
	    print!("#");
	} else {
	    print!(".");
	}
	x += 1;	
	if x % 40 == 0 {
	    x = 0;
	    println!();
	}
    }
}
