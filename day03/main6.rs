mod misc;

use itertools::Itertools;
use misc::{get_data,get_val};
use set::Set;

fn main() {
    let backpacks = get_data("./day03/input");
    let sum : u32 = backpacks.iter().tuples()
	.map(|(g1, g2, g3)| get_val(get_badge(g1, g2, g3).expect("no max")))
	.sum();
    
    println!("{}", sum);
}

fn get_badge(g1: &[u8], g2: &[u8], g3: &[u8]) -> Option<u8> {
    (Set::from(g1) & Set::from(g2) & Set::from(g3))
	.iter().next()
}

