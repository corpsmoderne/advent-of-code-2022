mod misc;
mod set1;

use misc::{get_val,get_data};
use set1::Set;

fn get_double(first: Vec<u8>, second: Vec<u8>) -> Option<u8> {
    let set1 = Set::new()
	.add_vec(&first);
    let set2 = Set::new()
	.add_vec(&second);
    let set3 = Set::new()
	.add_vec(&set1.get_uniques())
	.add_vec(&set2.get_uniques());
    
    set3.get_max()
}

fn main() {
    let backpacks = get_data("./day03/input");
    let sum: u32 = backpacks.into_iter()
	.map(|mut first| {
	    let second = first.split_off(first.len()/2);
	    get_val(get_double(first, second).expect("no double found"))
	})
	.sum();
    println!("{}", sum);
}

