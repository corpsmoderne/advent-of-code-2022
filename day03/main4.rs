mod misc;
mod set1;

use itertools::Itertools;
use misc::{get_data,get_val};
use set1::Set;

fn main() {
    let backpacks = get_data("./day03/input");
    let sum : u32 = backpacks.iter().tuples()
	.map(|(g1, g2, g3)| get_val(get_badge(g1, g2, g3).expect("no max")))
	.sum();
    
    println!("{}", sum);
}

fn get_badge(g1: &[u8], g2: &[u8], g3: &[u8]) -> Option<u8> {
    let set1 = Set::new().add_vec(g1);
    let set2 = Set::new().add_vec(g2);
    let set3 = Set::new().add_vec(g3);

    let set4 = Set::new()
	.add_vec(&set1.get_uniques())
	.add_vec(&set2.get_uniques())
	.add_vec(&set3.get_uniques());
    
    set4.get_max()
}

