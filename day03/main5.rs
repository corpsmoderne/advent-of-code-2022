mod misc;

use misc::{get_val,get_data};
use set::Set;

fn get_double(first: &[u8], second: &[u8]) -> Option<u8> {
    (Set::from(first) & Set::from(second))
	.iter().next()
}

fn main() {
    let backpacks = get_data("./day03/input");
    let sum: u32 = backpacks.into_iter()
	.map(| group | {
	    let half = group.len()/2;
	    let first = &group[0..half];
	    let second = &group[half..];
	    get_val(get_double(first, second).expect("no double found"))
	})
	.sum();
    println!("{}", sum);
}

