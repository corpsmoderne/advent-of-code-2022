#[derive(Debug,Clone)]
pub struct Set {
    tbl: [u32 ; 256]
}

impl Set {
    pub fn new() -> Self {
	Self { tbl: [ 0 ; 256 ] }
    }
    
    pub fn add_vec(mut self, v: &[u8]) -> Self {
	for b in v {
	    self.tbl[*b as usize] += 1;
	}
	self
    }

    pub fn get_uniques(&self) -> Vec<u8> {
	self.tbl.iter().enumerate()
	    .filter(| (_i, &n) | n > 0)
	    .map(| (i, _n) | i as u8)
	    .collect()
    }	

    pub fn get_max(&self) -> Option<u8> {
	let (i, n) = self.tbl.iter().enumerate()
	    .fold((0, 0), | (i, n), (j, &m) | {
		if m > n {
		    (j, m)
		} else {
		    (i, n)
		}
	    });
	if n == 0 {
	    None
	} else {
	    Some(i as u8)
	}
    }
}
