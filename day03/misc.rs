use std::fs::File;
use std::io::{self,BufRead};

pub fn get_data(filename: &str) -> Vec<Vec<u8>> {
    let file = File::open(filename).unwrap();
    io::BufReader::new(file).lines()
	.map(|l| l.unwrap().bytes().collect())
	.collect()
}

pub fn get_val(b: u8) -> u32 {
    if (b'a'..=b'z').contains(&b) {
	(b - b'a' + 1) as u32
    } else if (b'A'..=b'Z').contains(&b) { 
	(b - b'A' + 27) as u32
    } else {
	panic!("bad input");
    }
}
