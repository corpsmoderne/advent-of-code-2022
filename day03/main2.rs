mod misc;

use std::collections::HashSet;
use std::iter::FromIterator;
use itertools::Itertools;
use misc::{get_data,get_val};

fn main() {
    let backpacks = get_data("./day03/input");
    let sum : u32 = backpacks.iter().tuples()
	.map(|(g1, g2, g3)| {
	    let badge = get_badge(g1.to_vec(), g2.to_vec(), g3.to_vec())
		.expect("one must be found");
	    
	    get_val(badge)
	})
	.sum();
    
    println!("{}", sum);
}


fn get_badge(g1: Vec<u8>, g2: Vec<u8>, g3:Vec<u8>) -> Option<u8> {
    let set1 : HashSet<u8> = HashSet::from_iter(g1);
    let set2 : HashSet<u8> = HashSet::from_iter(g2);
    let set3 : HashSet<u8> = HashSet::from_iter(g3);
    
    set3.intersection(&set1.intersection(&set2)
    		      .copied()
		      .collect())
	.next().copied()
}

