mod misc;

use std::collections::HashSet;
use std::iter::FromIterator;
use misc::{get_val,get_data};

fn main() {
    let backpacks = get_data("./day03/input");
    let sum: u32 = backpacks.into_iter()
	.map(|mut first| {
	    let second = first.split_off(first.len()/2);
	    get_val(get_double(first, second).expect("no double found"))
	})
	.sum();
    println!("{}", sum);
}

fn get_double(first: Vec<u8>, second: Vec<u8>) -> Option<u8> {
    let first_set : HashSet<u8> = HashSet::from_iter(first);
    let second_set : HashSet<u8> = HashSet::from_iter(second);

    first_set
	.intersection(&second_set)
	.next().copied()
}

