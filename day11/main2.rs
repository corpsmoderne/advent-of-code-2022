mod misc;

use misc::{load_monkeys,apply_rounds};

fn main() {
    let monkeys_vec = load_monkeys("./day11/input");
    let max: u64 = monkeys_vec.iter()
	.map(| m | m.test_div)
	.product();
    
    let result = apply_rounds(monkeys_vec, 10_000, Some(max));
    
    println!("Monkey business: {}", result);
}
