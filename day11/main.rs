mod misc;

use misc::{load_monkeys,apply_rounds};

fn main() {
    let monkeys_vec = load_monkeys("./day11/input");
    let result = apply_rounds(monkeys_vec, 20, None);
    println!("Monkey business: {}", result);
}
