use std::fs;

use nom::{
    IResult,
    multi::{many1,separated_list1},
    sequence::{tuple,separated_pair,delimited,preceded},
    bytes::complete::{tag, take_while},
    combinator::{map,map_res},
    branch::alt,
    character::complete::multispace1
};

#[derive(Debug,Clone)]
enum Operator {
    Add,
    Mul
}

#[derive(Debug,Clone)]
enum Operand {
    Old,
    Num(u64)
}

#[derive(Debug,Clone)]
struct Operation {
    operator: Operator,
    operand: Operand
}

impl Operation {
    fn apply(&self, item: u64) -> u64 {
	let operand = match self.operand {
	    Operand::Old => item,
	    Operand::Num(n) => n
	};
	match self.operator {
	    Operator::Add => item + operand,
	    Operator::Mul => item * operand
	}
    }
}

#[derive(Debug,Clone)]
pub struct Monkey {
    pub id: u64,
    pub items: Vec<u64>,
    operation: Operation,
    pub test_div: u64,
    if_true: u64,
    if_false: u64,
    pub items_inspected: usize
}

impl Monkey {
    pub fn turn(&mut self, max: Option<u64>) -> Vec<(u64,u64)> {
	let mut dispatch = vec![];
	
	for item in &self.items {
	    let worry = if let Some(max) = max {
		self.operation.apply(*item) % max
	    } else {
		self.operation.apply(*item) / 3
	    };
	    dispatch.push(if worry % self.test_div == 0 {
		(self.if_true, worry)
	    } else {
		(self.if_false, worry)
	    });
	}
	self.items_inspected += self.items.len();
	self.items.clear();
	dispatch
    }
}

fn parse_u64(input: &str) -> IResult<&str, u64> {
    map_res(
	take_while(|c: char| c.is_ascii_digit()),
	|input: &str| input.parse::<u64>() //u64::from_str_radix(input, 10)
    )(input)
}

fn parse_items(input: &str) -> IResult<&str, Vec<u64>> {
    separated_list1(tag(", "), parse_u64)(input)
}

fn parse_operator(input: &str) -> IResult<&str, Operator> {
    alt((
	map(tag("+"), |_| Operator::Add),
	map(tag("*"), |_| Operator::Mul)
    ))(input)
}

fn parse_operand(input: &str) -> IResult<&str, Operand> {
    alt((
	map(tag("old"), |_| Operand::Old),
	map(parse_u64, Operand::Num)
    ))(input)
}

fn parse_operation(input: &str) -> IResult<&str, Operation> {
    map(separated_pair(parse_operator, tag(" "), parse_operand),
	|(operator, operand)| Operation { operator, operand })(input)
}

fn parse_monkey(input: &str) -> IResult<&str, Monkey> {
    map(
	tuple((delimited(tag("Monkey "), parse_u64,
			 preceded(tag(":"), multispace1)),
	       delimited(tag("Starting items: "), parse_items, multispace1),
	       delimited(tag("Operation: new = old "), parse_operation,
			 multispace1),
	       delimited(tag("Test: divisible by "), parse_u64, multispace1),
	       delimited(tag("If true: throw to monkey "), parse_u64,
			 multispace1),
	       delimited(tag("If false: throw to monkey "), parse_u64,
			 multispace1)
	)),
	| ( id, items, operation, test_div, if_true, if_false) |
	Monkey { id, items, operation, test_div,
		 if_true, if_false, items_inspected: 0 })(input)
}

pub fn apply_rounds(
    mut monkeys: Vec<Monkey>,
    rounds: u32,
    max: Option<u64>
) -> usize {
    for round in 1..=rounds {
	for id in 0..monkeys.len() {
	    let dispatch = monkeys[id].turn(max);
	    for (id, item) in dispatch {
		monkeys[id as usize].items.push(item);
	    }
	}
	println!(concat!("After round {}, the monkeys are holding ",
			 "items with these worry levels:"), round);
	for monkey in &monkeys {
	    println!("Monkey {} : {:?}", monkey.id, monkey.items);
	}
	println!();
    }
    
    for monkey in &monkeys {
	println!("Monkey {} inspected items {} times.",
		 monkey.id, monkey.items_inspected);
    }
    println!();

    let mut results : Vec<usize> = monkeys.iter()
	.map(| m | m.items_inspected)
	.collect();
    results.sort();
    results.reverse();
    results[0] * results[1]
}

pub fn load_monkeys(file: &str) -> Vec<Monkey> {
    let buf = fs::read_to_string(file).expect("can't read file");
    let (input, monkeys) = many1(parse_monkey)(&buf).expect("can't parse");
    if !input.is_empty() {
	panic!("can't parse: \"{}\"", input);
    }
    monkeys
}
