mod misc;

use misc::{get_data,parse_packet,Packet};

fn main() {
    let lines = get_data("./day13/input");
    let mut packets = vec![
        Packet::List(vec![Packet::List(vec![Packet::Num(2)])]),
        Packet::List(vec![Packet::List(vec![Packet::Num(6)])]),
    ];
    for chunk in lines.chunks(3) {
        let pack1 = parse_packet(&chunk[0]).unwrap().1;
        let pack2 = parse_packet(&chunk[1]).unwrap().1;
        packets.push(pack1);
        packets.push(pack2);
    }

    packets.sort_by(| p1, p2 | p1.check(p2));

    let res : usize = packets.into_iter()
        .enumerate()
        .filter(| (_, p) | {
            let Packet::List(l1) = p else { return false };
            if l1.len() != 1 { return false; }
            let Packet::List(l2) = &l1[0] else { return false };
            if l2.len() != 1 { return false; }
            let Packet::Num(n) = l2[0] else { return false };
            n == 2 || n == 6
        })
        .map(| (idx, _ ) | idx+1)
        .product();
    
    println!("{:?}", res);
}
