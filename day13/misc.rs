use std::fs::File;
use std::io::{self,BufRead};
use nom::{
    IResult,
    multi::separated_list0,
    sequence::delimited,
    bytes::complete::{tag, take_while},
    combinator::{map,map_res},
    branch::alt,
};
use std::cmp::Ordering;

#[derive(Debug,Clone)]
pub enum Packet {
    Num(u8),
    List(Vec<Packet>)
}


impl Packet {
    pub fn check(&self, other: &Packet) -> Ordering {
        match (self, other) {
            (Packet::Num(n1), Packet::Num(n2)) => n1.partial_cmp(n2).unwrap(),
            (Packet::List(l1), Packet::List(l2)) => {
                let lst : Vec<Ordering> = l1.iter()
                    .zip(l2)
                    .map(| (p1, p2) | p1.check(p2))
                    .collect();
                for status in lst {
                    match status {
                        Ordering::Equal => { },
                        s => return s
                    }
                }
                l1.len().partial_cmp(&l2.len()).unwrap()
            },
            (p1@Packet::Num(_), p2@Packet::List(_)) => {
                (Packet::List(vec![p1.clone()])).check(p2)
            },
            (p1@Packet::List(_), p2@Packet::Num(_)) => {
                p1.check(&Packet::List(vec![p2.clone()]))
            }
        }
    }
}

fn parse_u8(input: &str) -> IResult<&str, u8> {
    map_res(
        take_while(|c: char| c.is_ascii_digit()),
        |input: &str| input.parse()
    )(input)
}

fn parse_list(input: &str) -> IResult<&str, Vec<Packet>> {
    delimited(tag("["),
              separated_list0(tag(","), parse_packet),
              tag("]"))(input)
}

pub fn parse_packet(input: &str) -> IResult<&str, Packet> {
    alt((
        map(parse_list, Packet::List),
        map(parse_u8, Packet::Num)
    ))(input)
}

pub fn get_data(filename: &str) -> Vec<String> {
    let file = File::open(filename).unwrap();
    io::BufReader::new(file).lines()
	.map(|l| l.unwrap())
	.collect()
}

