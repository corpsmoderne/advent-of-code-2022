mod misc;

use misc::{get_data,parse_packet};

fn main() {
    let lines = get_data("./day13/input");
    let mut sum = 0;
    for (idx, chunk) in lines.chunks(3).into_iter().enumerate() {
        let pack1 = parse_packet(&chunk[0]).unwrap().1;
        let pack2 = parse_packet(&chunk[1]).unwrap().1;
        let res = pack1.check(&pack2);
        // println!("[{}]\t{:?}\n\t{:?}\n\t=> {:?}\n",
        //        idx+1, pack1, pack2, &res);
        match res {
            std::cmp::Ordering::Less => { sum += idx+1 },
            std::cmp::Ordering::Equal => panic!("undecided"),
            _ => { }
        };
    }
    println!("=> {sum}");
}
