mod misc;

use misc::{get_data,make_map,print_map,Map,Point,ROOT};

fn next_sand(map: &Map, sand: Point) -> Option<Point> {
    match map.map.get(&sand) {
        None | Some('.') => Some(sand),
        _ => None
    }
}

fn drop_sand(map: &mut Map) -> bool {
    let mut sand = ROOT;
    if map.map[&sand] == 'o' {
        return false;
    }
    while sand.1 < map.pmax.1 {
        if let Some(new_sand) = next_sand(map, (sand.0, sand.1+1))
            .or_else(|| next_sand(map, (sand.0-1, sand.1+1)))
            .or_else(|| next_sand(map, (sand.0+1, sand.1+1))) {
                sand = new_sand;
            } else {
                break;
            }
    }
    map.map.insert(sand, 'o');
    true
}

fn main() {
    let lines = get_data("./day14/input");
    let mut map = make_map(&lines);
    let mut counter = 0;

    print_map(&map);
    while drop_sand(&mut map) {
        counter += 1;
    }
    print_map(&map);    
    println!("{counter}\n");
}
