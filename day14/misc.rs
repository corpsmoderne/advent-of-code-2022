use std::fs::File;
use std::io::{self,BufRead};
use nom::{
    IResult,
    multi::separated_list1,
    sequence::{tuple,preceded},
    bytes::complete::{tag, take_while},
    combinator::map_res,
};
use std::collections::HashMap;

pub type Point = (u32, u32);
type Line = Vec<Point>;

pub struct Map {
    pub map: HashMap<Point, char>,
    pub pmin: Point,
    pub pmax: Point
}

pub const ROOT : Point = (500, 0);

fn parse_u32(input: &str) -> IResult<&str, u32> {
    map_res(take_while(|c: char| c.is_ascii_digit()),
            |input: &str| input.parse())(input)
}

fn parse_line(input: &str) -> IResult<&str, Line> {
    let parse_point = tuple((parse_u32, preceded(tag(","), parse_u32)));
    separated_list1(tag(" -> "), parse_point)(input)
}

fn add_horizontal_line(points: &mut Vec<Point>, a: &Point, b: &Point) {
    let (from, to) = if a.0 < b.0 { (a, b) } else { (b, a) };
    for x in from.0..=to.0 {
        points.push((x, from.1));
    }
}

fn add_vertical_line(points: &mut Vec<Point>, a: &Point, b: &Point) {
    let (from, to) = if a.1 < b.1 { (a, b) } else { (b, a) };
    for y in from.1..=to.1 {
        points.push((from.0, y));
    }
}

pub fn make_map(lines: &[String]) -> Map {
    let lines : Vec<Line> = lines.iter()
        .map(|l | parse_line(l).unwrap().1)
        .collect();
    let mut points: Vec<Point> = vec![];

    for line in lines {
        for (p1, p2) in line.iter().zip(&line[1..]) {
            if p1.1 == p2.1 {
                add_horizontal_line(&mut points, p1, p2);
            } else if p1.0 == p2.0 {
                add_vertical_line(&mut points, p1, p2);                
            } else {
                panic!("can't do diagonals..");
            }
        }
    }
    let pmin = (points.iter().map(| (x, _) | x).min().unwrap().min(&ROOT.0),
                points.iter().map(| (_, y) | y).min().unwrap().min(&ROOT.1));
    let pmax = (points.iter().map(| (x, _) | x).max().unwrap().max(&ROOT.0),
                points.iter().map(| (_, y) | y).max().unwrap().max(&ROOT.1));
    let mut map: HashMap<Point, char> = HashMap::new();
    for y in *pmin.1..=*pmax.1 {
        for x in *pmin.0..=*pmax.0 {
            map.insert((x,y), '.');
        }
    }
    for p in &points {
        map.insert(*p, '#');
    }
    map.insert(ROOT, '+');
    Map { map, pmin: (*pmin.0, *pmin.1), pmax: (*pmax.0+1, *pmax.1+1) }
}

pub fn print_map(map: &Map) {
    let mut s = String::new();
    for y in map.pmin.1..map.pmax.1 {
        for x in map.pmin.0..map.pmax.0 {
            s.push_str(&format!("{}", map.map[&(x, y)]));
        }
        s.push('\n');
    }
    println!("{}", s);
}

pub fn get_data(filename: &str) -> Vec<String> {
    let file = File::open(filename).unwrap();
    io::BufReader::new(file).lines()
	.map(|l| l.unwrap())
	.collect()
}

