mod misc;

use misc::{get_data,make_map,print_map,Map,ROOT};

fn drop_sand(map: &mut Map) -> bool {
    let mut sand = ROOT;
    
    loop {
        match map.map.get(&(sand.0, sand.1+1)) {
            None => { return false },
            Some('.') => { sand.1 += 1 ; continue },
            _ => {}
        }
        match map.map.get(&(sand.0-1, sand.1+1)) {
            None => { return false },
            Some('.') => { sand.0 -= 1 ; sand.1 += 1 ; continue },
            _ => {}
        }
        match map.map.get(&(sand.0+1, sand.1+1)) {
            None => { return false },
            Some('.') => { sand.0 += 1; sand.1 += 1 ; continue },
            _ => {}
        }
        break
    }
    map.map.insert(sand, 'o');
    true
}

fn main() {
    let lines = get_data("./day14/input");
    let mut map = make_map(&lines);
    let mut counter = 0;
    print_map(&map);
    while drop_sand(&mut map) {
        print_map(&map);
        counter += 1;
        println!("{counter}\n");
    }
}
