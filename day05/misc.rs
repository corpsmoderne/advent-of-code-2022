use std::fs::File;
use std::io::{self,BufRead};
use std::str::FromStr;

pub type Board = Vec<Vec<u8>>;

#[derive(Debug,Clone,Copy)]
pub struct Cmd {
    pub amount: usize,
    pub from: usize,
    pub to: usize
}

impl FromStr for Cmd {
    type Err = String;
    
    fn from_str(s: &str) -> Result<Cmd, String> {
	match s.split(' ').collect::<Vec<&str>>()[..] {
	    ["move", n, "from", from, "to", to] =>
		Ok(Cmd { amount: n.parse().unwrap(),
			 from: from.parse().unwrap(),
			 to: to.parse().unwrap() }),
	    _ => Err(String::from("bad input"))
	}
    }    
}

fn get_board_end(data: &[String]) -> Option<usize> {
    for (i,l) in data.iter().enumerate() {
	if l.is_empty() {
	    return Some(i)
	}
    }
    None
}

pub fn get_results(board: &mut Board) -> String {
    let mut result = String::new();
    for line in board {
	result.push(line.pop().expect("should not be empty here") as char);
    }
    result
}

pub fn get_board_and_commands(mut board_s: Vec<String>) -> (Board, Vec<Cmd>) {
    let limit = get_board_end(&board_s).expect("should be an empty line");
    let cmds_s = board_s.split_off(limit)[1..].to_vec();

    let board = make_board(&board_s);

    let cmds : Vec<Cmd> = cmds_s.iter()
	.map(| s | s.parse().expect("parse error"))
	.collect();
    (board, cmds)
}

fn make_board(board_s: &[String]) -> Board {
    let n = (board_s[0].len() + 1) / 4;
    let mut board = vec![ vec![] ; n ];
    
    for l in &board_s[0..board_s.len()-1] {
	for (i, line) in board.iter_mut().enumerate().take(n) {
	    let bytes : Vec<u8> = l.bytes().collect();
	    let m = 1 + i*4;
	    if bytes[m] != b' ' {
		line.push(bytes[m] as u8)
	    }
	}
    }
    for l in &mut board {
	l.reverse()
    }
    board
}

pub fn get_data(filename: &str) -> Vec<String> {
    let file = File::open(filename).unwrap();
    io::BufReader::new(file).lines()
	.map(|l| l.unwrap())
	.collect()
}

