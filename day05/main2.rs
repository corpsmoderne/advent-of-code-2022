mod misc;

use misc::{get_data,get_board_and_commands,get_results};

fn main() {
    let (mut board, cmds) = get_board_and_commands(get_data("./day05/input"));

    // apply commands
    for cmd in &cmds {
	let n = board[cmd.from-1].len()-cmd.amount;
	let mut tmp = board[cmd.from-1].split_off(n);
	board[cmd.to-1].append(&mut tmp);
    }    

    // get results
    println!("{}", get_results(&mut board));
}
