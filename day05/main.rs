mod misc;

use misc::{get_data,get_board_and_commands,get_results};

fn main() {
    let (mut board, cmds) = get_board_and_commands(get_data("./day05/input"));

    // apply commands
    for cmd in &cmds {
	for _ in 0..cmd.amount {
	    let c = board[cmd.from-1].pop().expect("should not be empty");
	    board[cmd.to-1].push(c);
	}
    }

    // get results
    println!("{}", get_results(&mut board));
}
