mod misc;

use misc::{get_data,get_sensors,make_map,get_sensor_y_range,Map};

fn find_hole(ranges: &[(i32,i32)], y: i32, map: &Map) -> Option<i32> {
    let mut max = 0;
    for i in 0..ranges.len()-1 {
	max = max.max(ranges[i].1);
	if max+2 == ranges[i+1].0 {
	    let x = max+1;
	    if map.map.get(&(x, y)).is_none() {		
		return Some(ranges[i].1+1)
	    }
	}
    }
    None
}

fn main() {
    let lines = get_data("./day15/input");
    let sensors = get_sensors(&lines);
    let map = make_map(&sensors);
    let max = 4_000_000;
    
    for y in 0..=max {
	let mut ranges: Vec<(i32,i32)> = vec![];
	for sensor in &sensors {
	    let r = get_sensor_y_range(y, sensor);
	    if let Some(r) = r {
		ranges.push(r);
	    }
	}
	ranges.sort_by(| (x0, _), (x2, _) | x0.partial_cmp(&x2).unwrap());
	if let Some(x) = find_hole(&ranges, y, &map) {
	    println!("{} {} => {}", x, y, x as i64 * max as i64 + y as i64);
	}
	
    }
}
