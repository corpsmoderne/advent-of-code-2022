mod misc;

use misc::{get_data,get_sensors,get_sensor_y_range};


fn main() {
    let lines = get_data("./day15/input");
    let sensors = get_sensors(&lines);
    let y = 2000000;

    let mut range : Option<(i32,i32)> = None;
    for sensor in &sensors {
	let r = get_sensor_y_range(y, sensor);
	if let Some((x0,x1)) = r {
	    if let Some((x2,x3)) = range {
		range = Some((x0.min(x2), x1.max(x3)))
	    } else {
		range = r;
	    }
	}
    }
    let Some((x0,x1)) = range else {
	panic!("not found");
    };
    println!("{}", (x1-x0).abs());
}
