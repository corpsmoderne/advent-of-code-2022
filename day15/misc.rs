use std::fs::File;
use std::io::{self,BufRead};
use std::collections::HashMap;

use nom::{
    IResult,
    sequence::{tuple,preceded},
    bytes::complete::{tag, take_while},
    combinator::{map,map_res},
};

pub type Point = (i32, i32);

#[derive(Debug,Clone)]
pub struct Map {
    pub map: HashMap<Point, char>,
    pub min_p: Point,
    pub max_p: Point
}

#[derive(Debug,Clone)]
pub struct Sensor {
    pub idx: u8,
    pub pos: Point,
    pub beacon: Point,
    pub distance: u32
}

fn parse_i32(input: &str) -> IResult<&str, i32> {
    map_res(take_while(|c: char| (c=='-' || c.is_ascii_digit())),
            |input: &str| input.parse())(input)
}

fn parse_sensor(idx: u8, input: &str) -> IResult<&str, Sensor> {
    map(
	tuple((preceded(tag("Sensor at x="), parse_i32),
	       preceded(tag(", y="), parse_i32),
	       preceded(tag(": closest beacon is at x="), parse_i32),
	       preceded(tag(", y="), parse_i32))),
	| (x0, y0, x1, y1) | {
	    let pos = (x0, y0);
	    let beacon = (x1, y1);
	    let distance = mdist(pos, beacon);
	    Sensor { idx, pos, beacon, distance }
	})(input)
	
}

pub fn mdist(p1: Point, p2: Point) -> u32 {
    ((p1.0-p2.0).abs() + (p1.1-p2.1).abs()) as u32
}

pub fn get_sensor_y_range(y: i32, sensor: &Sensor) -> Option<(i32, i32)> {
    let ydiff: i32 = (sensor.pos.1-y).abs();
    if ydiff as u32 > sensor.distance {
	return None
    }
    Some((sensor.pos.0 - (sensor.distance as i32 - ydiff),
	 sensor.pos.0 + (sensor.distance as i32 - ydiff)))
}

pub fn get_sensors(lines: &[String]) -> Vec<Sensor> {
    let mut v = vec![];
    for (i, line) in lines.iter().enumerate() {
	let Ok(("", sensor)) = parse_sensor(i as u8, line) else {
	    panic!("parse error");
	};
	v.push(sensor);
    }
    v
}

#[allow(dead_code)]
pub fn make_map(sensors: &[Sensor]) -> Map {
    let mut map = HashMap::new();
    let mut min_p = sensors[0].pos;
    let mut max_p = sensors[0].pos;
    
    for sensor in sensors {
	map.insert(sensor.pos, 'S');
	map.insert(sensor.beacon, 'B');
	min_p = (min_p.0.min(sensor.pos.0).min(sensor.beacon.0),
		 min_p.1.min(sensor.pos.1).min(sensor.beacon.1));
	max_p = (max_p.0.max(sensor.pos.0).max(sensor.beacon.0),
		 max_p.1.max(sensor.pos.1).max(sensor.beacon.1));
    }
    Map { map, min_p, max_p }
}

#[allow(dead_code)]
pub fn print_map(map: &Map) {
    for y in map.min_p.1..=map.max_p.1 {
	for x in map.min_p.0..=map.max_p.0 {
	    if let Some(c) = map.map.get(&(x, y)) {
		print!("{} ", c);
	    } else {
		print!(". ");
	    }
	}
	println!();
    }
    println!();    
}

pub fn get_data(filename: &str) -> Vec<String> {
    let file = File::open(filename).unwrap();
    io::BufReader::new(file).lines()
	.map(|l| l.unwrap())
	.collect()
}

