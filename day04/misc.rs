use std::fs::File;
use std::io::{self,BufRead};

pub fn get_data(filename: &str) -> Vec<String> {
    let file = File::open(filename).unwrap();
    io::BufReader::new(file).lines()
	.map(|l| l.unwrap())
	.collect()
}

pub fn to_pairs_of_pairs(data: &[String]) -> Vec<((u8,u8),(u8,u8))> {
    data
	.iter()
	.map(| s| {
	    let tbl : Vec<(u8,u8)> = s.split(',')
		.map(| p | {
		    let pair : Vec<u8> = p.split('-')
			.map(| s | s.parse::<u8>().expect("bad input"))
			.collect();
		    to_pair(&pair)
		})
		.collect();
	    to_pair(&tbl)
	}).collect()
}

fn to_pair<T: Clone>(v: &[T]) -> (T, T) {
    match v {
	[p1, p2] => ((*p1).clone(), (*p2).clone()),
	_ => panic!("bad format")
    }
}
						       
						       

						       
						       
