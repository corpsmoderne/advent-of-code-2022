mod misc;

use misc::{to_pairs_of_pairs,get_data};
use set::Set;

fn main() {
    let data = get_data("./day04/input");
    let pairs = to_pairs_of_pairs(&data);
    let sum = pairs.iter()
	.map(|pair| {
	    let mut set1 = Set::new();
	    for i in (pair.0.0)..=(pair.0.1) {
		set1.set(i);
	    }
	    let mut set2 = Set::new();
	    for i in (pair.1.0)..=(pair.1.1) {
		set2.set(i);
	    }
	    !(set1 & set2).is_empty()
	})
	.filter(|&b| b)
	.count();
    println!("{sum}");
}
