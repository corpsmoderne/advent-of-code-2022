mod elves;
use elves::get_elves;

fn main() {
    let mut elves = get_elves("day01/input");
    if elves.is_empty() {
	panic!("no elves given");
    }
    elves.sort();
    elves.reverse();

    let sum : u32 = elves[0..3].iter().sum();
    
    println!("{}", sum);
}

