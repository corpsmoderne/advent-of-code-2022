use std::fs::File;
use std::io::{self,BufRead};

pub fn get_elves(filename: &str) -> Vec<u32> {
    let file = File::open(filename).unwrap();
    let lines = io::BufReader::new(file).lines();

    let mut elves : Vec<u32> = vec![];
    let mut acc = 0_u32;

    for l in lines {
	let line = l.expect("this should not happen at all");
	if line.is_empty() {
	    elves.push(acc);
	    acc = 0;
	} else {
	    acc += line.parse::<u32>().unwrap();
	}
    }
    if acc != 0 {
	elves.push(acc);
    }
    elves
}
