mod elves;
use elves::get_elves;

fn main() {
    let elves = get_elves("day01/input");

    if elves.is_empty() {
	panic!("no elves given");
    }
    let best = elves.into_iter().max().expect("not enough values");
    println!("{}", best);
}

