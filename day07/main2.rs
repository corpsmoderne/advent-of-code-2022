mod misc;

use misc::{get_data,make_tree};

fn main() {
    let lines = get_data("./day07/input");
    let mut root = make_tree(&lines);
    root.calc_sizes();

    let Some(mut flat) = root.flatten() else { panic!("found nothin' :(") };
    flat.sort_by(| (_, s1), (_, s2) | s1.partial_cmp(s2).unwrap());

    for (_, s) in flat {
	if 70000000 - (root.size - s) > 30000000 {
	    println!("{}", s);
	    break;
	}
    }
}
