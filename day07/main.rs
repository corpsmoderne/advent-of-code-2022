mod misc;

use misc::{get_data,make_tree};

fn main() {
    let lines = get_data("./day07/input");
    let mut root = make_tree(&lines);
    root.calc_sizes();

    let Some(flat) = root.flatten() else { panic!("found nothin' :(") };
    let sum : usize = flat.iter()
	.map(| (_, s) | *s )
	.filter(| s | *s < 100000).sum();
    println!("{:?}", sum);
}
