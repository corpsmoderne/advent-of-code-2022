use std::fs::File;
use std::io::{self,BufRead};
use std::collections::HashMap;

#[derive(Debug,Clone)]
pub struct DirEnt {
    pub name: String,
    pub size: usize,
    pub attr: Attr,
}

#[derive(Debug,Clone)]
pub enum Attr {
    File,
    Dir(HashMap<String, DirEnt>)
}

impl DirEnt {
    pub fn new(tbl: &[&str]) -> DirEnt {
	match tbl {
	    ["dir", name] => {
		DirEnt { name: name.to_string(), size: 0,
			 attr: Attr::Dir(HashMap::new())
		}
	    },
	    [size, name] => {
		DirEnt { name: name.to_string(), size: size.parse().unwrap(),
			 attr: Attr::File,
		}		
	    },
	    _ => panic!("bad input")
	}
    }
    
    pub fn flatten(&mut self) -> Option<Vec<(String,usize)>> {
	let Attr::Dir(ref mut dirs) = self.attr else { return None };
	let mut flat: Vec<(String,usize)> = vec![];
	for (k,v) in dirs {
	    let Some(mut res) = v.flatten() else { continue };
	    flat.push((k.clone(), v.size));
	    flat.append(&mut res)
	}
	Some(flat)
    }

    pub fn calc_sizes(&mut self) -> usize {
	match self.attr {
	    Attr::File => self.size,
	    Attr::Dir(ref mut dirs) => {
		let mut size = 0;
		for v in dirs.values_mut() {
		    size += v.calc_sizes();
		}
		self.size = size;
		size
	    }
	}
    }
    
    fn cd(&mut self, to: &str) -> &mut Self {
	let Attr::Dir(ref mut dirs) = self.attr else {
	    panic!("not a dir!");
	};
	dirs.get_mut(to).expect("file not found")
    }
}

pub fn make_tree(lines: &[String]) -> DirEnt {
    let mut root = DirEnt { name: "/".to_string(),
			    size: 0,
			    attr: Attr::Dir(HashMap::new())
    };
    let mut current = &mut root;
    let mut path : Vec<String> = vec![];
    
    for line in &lines[1..] {
	let tbl: Vec<&str> = line.split(' ').collect();
	if tbl[0] == "$" {
	    match tbl[1..] {
		["cd", ".."] => {
		    _ = path.pop();
		    current = &mut root;
		    for to in &path {
			current = current.cd(to)
		    }
		},
		["cd", x] => {
		    path.push(x.to_string());
		    current = current.cd(x);
		},
		_ => { }
		
	    }
	} else {
	    let file = DirEnt::new(&tbl);
	    let Attr::Dir(ref mut files) = current.attr else {
		panic!("not a dir!")
	    };
	    files.insert(file.name.clone(), file);
	}
    }
    root
}

pub fn get_data(filename: &str) -> Vec<String> {
    let file = File::open(filename).unwrap();
    io::BufReader::new(file).lines()
	.map(|l| l.unwrap())
	.collect()
}

