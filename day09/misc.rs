use std::fs::File;
use std::io::{self,BufRead};
//use std::collections::HashMap;

#[derive(Debug,Clone,Copy,Eq, Hash, PartialEq)]
pub struct Pos(pub i32, pub i32);

pub fn get_head_moves(lines: &[String]) -> Vec<Pos> {
    let mut turtle = Pos(0,0);
    let mut moves : Vec<Pos> = vec![];
    
    for line in lines {
	let tbl : Vec<&str> = line.split(' ').collect();
	let (d, n) = match &tbl[..] {
	    [d, n] => (d, n.parse::<u8>().unwrap() ),
	    _ => panic!("bad input")
	};
	let dir = match *d {
	    "R" => Pos(1,0),
	    "L" => Pos(-1,0),
	    "U" => Pos(0,-1),
	    "D" => Pos(0,1),
	    _ => panic!("bad input")
	};
	for _ in 0..n {
	    turtle.0 += dir.0;
	    turtle.1 += dir.1;
	    moves.push(turtle);
	}
    }
    moves
}

pub fn get_data(filename: &str) -> Vec<String> {
    let file = File::open(filename).unwrap();
    io::BufReader::new(file).lines()
	.map(|l| l.unwrap())
	.collect()
}

