mod misc;

use misc::{get_data,get_head_moves,Pos};
use std::collections::HashSet;

fn main() {
    let lines = get_data("./day09/input");
    let mut tail_moves : HashSet<Pos> = HashSet::new();
    let mut tails : Vec<Pos> = vec![Pos(0,0) ; 9];

    let head_moves = get_head_moves(&lines);
    for pos in head_moves {
	move_rope(&mut tails[0], &pos);
	for i in 1..tails.len() {
	    let pos = tails[i-1];
	    move_rope(&mut tails[i], &pos);
	}
	/*
	for y in 0..5 {
	    for x in 0..=5 {
		if pos.0 == x && pos.1 +4 == y {
		    print!("H");
		} else if tail.0 == x && tail.1 +4 == y {
		    print!("T");
		} else if x == 0 && y == 4 {
		    print!("s");
		} else {
		    print!(".");
		}
	    }
	    println!();
	}
	println!();	
	 */
	tail_moves.insert(tails[8]);
    }
    println!();
    for pos in &tail_moves {
	println!("{:?}", pos);
    }
    println!("{}", tail_moves.len());
}

fn move_rope(tail: &mut Pos, pos: &Pos) {
    let d = (((tail.0-pos.0).pow(2) + (tail.1-pos.1).pow(2)) as f32).sqrt();
    if d > 1.5 {
	if tail.0 < pos.0 {
	    tail.0 += 1;
	}
	if tail.0 > pos.0 {
	    tail.0 -= 1;
	}
	if tail.1 > pos.1 {
	    tail.1 -= 1;
	}
	if tail.1 < pos.1 {
	    tail.1 += 1;
	}
    }
}
