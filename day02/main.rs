use std::fs::File;
use std::io::{self,BufRead};

#[derive(Debug,Clone)]
enum Shape {
    Rock,
    Paper,
    Scissors
}

impl Shape {
    fn score(&self) -> u32 {
	match self {
	    Shape::Rock => 1,
	    Shape::Paper => 2,
	    Shape::Scissors => 3,
	}
    }

    fn fight(&self, op: &Self) -> u32 {
	match (self, op) {
	    (Shape::Rock, Shape::Scissors) |
	    (Shape::Paper, Shape::Rock) |
	    (Shape::Scissors, Shape::Paper) => 6,
	    (Shape::Rock, Shape::Paper) |
	    (Shape::Paper, Shape::Scissors) |
	    (Shape::Scissors, Shape::Rock) => 0,
	    _ => 3
	}
    }
}

fn get_data(filename: &str) -> Vec<(Shape,Shape)> {
    let file = File::open(filename).unwrap();
    let lines = io::BufReader::new(file).lines();
    let mut res = vec![];
    
    for l in lines {
	let line = l.unwrap();
	if line.is_empty() {
	    continue;
	}
	let tbl : Vec<&str> = line.split(' ').collect();
	let (a, b) = match tbl[..] {
	    [a, b] => (a, b),
	    _ => panic!("should be always 2 elements here")
	};
	let x = match a {
	    "A" => Shape::Rock,
	    "B" => Shape::Paper,
	    "C" => Shape::Scissors,
	    _ => panic!("unknow char")
	};
	let y = match b {
	    "X" => Shape::Rock,
	    "Y" => Shape::Paper,
	    "Z" => Shape::Scissors,
	    _ => panic!("unknow char")
	};
	res.push((x, y));
    }
    res
}

fn main() {
    let hands = get_data("./day02/input");
    let mut score: u32 = 0;
    
    for (op, you) in hands {
	score += you.score() + you.fight(&op);
    }
    println!("{}", score);
}

