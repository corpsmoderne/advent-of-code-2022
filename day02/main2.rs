use std::fs::File;
use std::io::{self,BufRead};

#[derive(Debug,Clone,Copy)]
enum Shape {
    Rock,
    Paper,
    Scissors
}

#[derive(Debug,Clone,Copy)]
enum Res {
    Win,
    Lose,
    Draw
}

impl Shape {
    fn score(&self) -> u32 {
	match self {
	    Shape::Rock => 1,
	    Shape::Paper => 2,
	    Shape::Scissors => 3,
	}
    }
}

impl Res {
    fn score(&self) -> u32 {
	match self {
	    Res::Win => 6,
	    Res::Draw => 3,
	    Res::Lose => 0,
	}
    }
    
    fn fight(&self, op: &Shape) -> Shape {
	match (self, op) {
	    (Res::Draw, a) => *a,
	    (Res::Lose, Shape::Rock) => Shape::Scissors,
	    (Res::Lose, Shape::Paper) => Shape::Rock,
	    (Res::Lose, Shape::Scissors) => Shape::Paper,
	    (Res::Win, Shape::Rock) => Shape::Paper,
	    (Res::Win, Shape::Paper) => Shape::Scissors,
	    (Res::Win, Shape::Scissors) => Shape::Rock
	}
    }

}

fn get_data(filename: &str) -> Vec<(Shape,Res)> {
    let file = File::open(filename).unwrap();
    let lines = io::BufReader::new(file).lines();
    let mut res = vec![];
    
    for l in lines {
	let line = l.unwrap();
	if line.is_empty() {
	    continue;
	}
	let tbl : Vec<&str> = line.split(' ').collect();
	let (a, b) = match tbl[..] {
	    [a, b] => (a, b),
	    _ => panic!("should be always 2 elements here")
	};
	let x = match a {
	    "A" => Shape::Rock,
	    "B" => Shape::Paper,
	    "C" => Shape::Scissors,
	    _ => panic!("unknow char")
	};
	let y = match b {
	    "X" => Res::Lose,
	    "Y" => Res::Draw,
	    "Z" => Res::Win,
	    _ => panic!("unknow char")
	};
	res.push((x, y));
    }
    res
}

fn main() {
    let hands = get_data("./day02/input");
    let mut score: u32 = 0;
    
    for (op, you) in hands {
	score += you.score() + you.fight(&op).score();
    }
    println!("{}", score);
}

