mod misc;

use misc::get_data;

fn main() {
    let lines = get_data("./day06/input");

    for line in &lines {
	let chars : Vec<char> = line.chars().collect();
	let zipped = chars.iter()
	    .zip(chars[1..].iter())
	    .zip(chars[2..].iter())
	    .zip(chars[3..].iter());
	
	for (idx, (((a,b),c),d)) in zipped.enumerate() {
	    if a != b && a != c && a != d &&
		b != c && b != d &&
		c != d {
		    println!("{}", idx+4);
		    break
		}
	}
    }
}
