mod misc;

use misc::get_data;
use set::Set;

fn main() {
    let lines = get_data("./day06/input");

    for line in &lines {
	let chars : Vec<u8> = line.bytes().collect();

	for i in 0..chars.len()-14 {
	    let slice = &chars[i..i+14];
	    let set = Set::from(slice);
	    if set.iter().count() == 14 {
		println!("{}", i+14);
		break
	    }
	}
    }
}
