use std::fs::File;
use std::io::{self,BufRead};

pub fn get_data(filename: &str) -> Vec<String> {
    let file = File::open(filename).unwrap();
    io::BufReader::new(file).lines()
	.map(|l| l.unwrap())
	.collect()
}

